package musicplayer.midi.controllers;

import musicplayer.controllers.IFacadePlayerTrack;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import java.util.Observer;

public class FacadePlayerMidiTrack implements IFacadePlayerTrack {

    private Sequencer sequencer;
    private boolean status;
    private EventListenerPlayerMidiTrack listener;

    /**
     * Constructor
     *
     * @param Sequencer
     */
    public FacadePlayerMidiTrack(Sequence seq) {
	System.out.println("-> Player midi track created");
        this.status = false;
        this.listener = new EventListenerPlayerMidiTrack(this);
        try {
            this.sequencer = MidiSystem.getSequencer();
            this.sequencer.open();
            this.sequencer.setSequence(seq);
            this.addListeners();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * {@inheritDoc}
     */
    public void stop() {
        this.sequencer.stop();
        this.status = false;
        this.sequencer.setMicrosecondPosition(0);
    }

    /**
     * {@inheritDoc}
     */
    public boolean togglePause() {
        // player is stopped
        if (!this.status) {
            this.sequencer.start();
        } else {
            this.sequencer.stop();
        }
        this.status = !this.status;
        return this.status;
    }

    /**
     * Get position
     *
     * @return the position
     */
    public long getPosition() {
        return this.sequencer.getMicrosecondPosition();
    }

    @Override
    public void addObserver(Observer obs) {
        this.listener.addObserver(obs);
    }

    public void addListeners() {
	System.out.println("Binding listeners to sequence");
        sequencer.addControllerEventListener(this.listener, new int[] { 127 });
        sequencer.addMetaEventListener(this.listener);
    }
    public void setSequence(Sequence seq) {
        try {
            this.sequencer.setSequence(seq);
        } catch (InvalidMidiDataException e) {
            e.printStackTrace();
        }
    }
}
