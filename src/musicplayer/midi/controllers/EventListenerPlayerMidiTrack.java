package musicplayer.midi.controllers;

import java.util.Observable;
import javax.sound.midi.ControllerEventListener;
import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.ShortMessage;

public class EventListenerPlayerMidiTrack extends Observable implements ControllerEventListener, MetaEventListener {
    private FacadePlayerMidiTrack facadePlayerMidiTrack;
    public EventListenerPlayerMidiTrack(
	    FacadePlayerMidiTrack facadePlayerMidiTrack) {
	this.facadePlayerMidiTrack = facadePlayerMidiTrack;
    }

    @Override
    public void controlChange(ShortMessage event) {
	// Check if the control change
	this.setChanged();
	this.notifyObservers();
    }

    @Override
    public void meta(MetaMessage metaMsg) {
	// Check if the sequence is finished
	System.out.println("Meta event: "+metaMsg.getType());
        if (metaMsg.getType() == 0x2F) {
            this.setChanged();
            this.notifyObservers("END");
            this.facadePlayerMidiTrack.stop();
        }
    }
}
