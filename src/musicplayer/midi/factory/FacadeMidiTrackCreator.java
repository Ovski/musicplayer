package musicplayer.midi.factory;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

/**
 * FacadeMidiTrackCreator
 * 
 * Facade of the midi library to handle the track generation
 * 
 * @author pierre-jean.leger
 */
public class FacadeMidiTrackCreator {

	/**
	 * Generate a random midi sequence
	 * @return The generated sequence
	 */
	public Sequence generate() {
		try {
			Sequence seq = new Sequence(Sequence.PPQ, 4);
			Track piste = seq.createTrack();

			int r = 0;
			for (int i = 0; i < 100; i += 4) {

				r = (int) ((Math.random() * 50) + 1);

				piste.add(makeEvent(144, 1, r, 100, i));

				piste.add(makeEvent(176, 1, 127, 0, i));

				piste.add(makeEvent(128, 1, r, 100, i + 2));

			}

			return seq;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Create a midiEvent
	 * @param comd
	 * @param can
	 * @param un
	 * @param deux
	 * @param tic
	 * @return
	 */
	public MidiEvent makeEvent(int comd, int can, int un, int deux, int tic) {
		MidiEvent evenement = null;
		try {
			ShortMessage a = new ShortMessage();
			a.setMessage(comd, can, un, deux);
			evenement = new MidiEvent(a, tic);

		} catch (Exception e) {
		}
		return evenement;
	}
}
