package musicplayer.midi.factory;

import musicserver.service.IMusicService;

import java.io.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * MidiTrackFactory
 * 
 * Generate/Load a midi file
 * 
 * @author pierre-jean.leger
 *
 */
public class MidiTrackFactory {
	FacadeMidiTrackCreator midiTrackCreator = new FacadeMidiTrackCreator();

	/**
	 * Generate or load a midi file
	 * 
	 * @param mode
	 *            true : load a file throught a JFileChooser false : generate a
	 *            random midi
	 * @return
	 */
	public Sequence create(int mode) {
		Sequence sequence = null;
        switch(mode) {
            // Local file
            case 0:
                // Create a file chooser
                JFileChooser fc = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "MIDI Files", "mid", "midi");
                fc.setAcceptAllFileFilterUsed(false);
                fc.setFileFilter(filter);
                fc.setMultiSelectionEnabled(false);
                int returnVal = fc.showOpenDialog(null);

                // If no file has been selected
                if (returnVal != JFileChooser.APPROVE_OPTION) {
                    return null;
                }

                // Load the file
            break;
            // Generated file
            case 1:
                sequence = midiTrackCreator.generate();
            break;
            // Remote file
            case 2:
                try {
                    Registry registry = LocateRegistry.getRegistry("127.0.0.1", 25565);

                    IMusicService service = (IMusicService) registry.lookup("MusicService");

                    byte[] byteMidi = service.getMidiOfTheDay();

                    String tmpName = "midiOfTheDay.mid";
                    File midiFile = new File(tmpName);
                    if(!midiFile.exists()) {
                        midiFile.createNewFile();
                    } else {
                        midiFile.delete();
                        midiFile.createNewFile();
                    }
                    BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(midiFile.getName()));
                    output.write(byteMidi, 0, byteMidi.length);
                    output.flush();
                    output.close();

                    sequence = loadSequence(midiFile);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            break;
        }
		return sequence;
	}

    public Sequence loadSequence(File file) {
        Sequence sequence = null;
        try {
            sequence  = MidiSystem.getSequence(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sequence;
    }

    public Sequence loadSequence(InputStream stream) {
        Sequence sequence = null;
        try {
            sequence  = MidiSystem.getSequence(stream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sequence;
    }
}
