package musicplayer.controllers;

import musicplayer.midi.controllers.FacadePlayerMidiTrack;
import musicplayer.midi.factory.MidiTrackFactory;

import javax.sound.midi.Sequence;
import java.util.Observer;

/**
 * PlayerTrack
 * <p/>
 * Generic track player
 *
 * @author baptiste
 */
public class PlayerTrack {
    private static volatile IFacadePlayerTrack facadePlayerTrack = null;

    /**
     * Constructor
     */
    public PlayerTrack() {
        this.load(1);
    }

    /**
     * Load a track
     * mode = 0 -> Local file
     * mode = 1 -> Generated file
     * mode = 2 -> Remote file
     */
    public void load(int mode) {
        // TODO Add an abstraction layer for Sequence and allow to use MP3 file
        // MIDI engine
        if(true) {
            MidiTrackFactory trackFactory = new MidiTrackFactory();
            Sequence midiSequence = trackFactory.create(mode);
            PlayerTrack.facadePlayerTrack = PlayerTrack.getFacadePlayerTrackInstance(midiSequence);
            ((FacadePlayerMidiTrack)PlayerTrack.facadePlayerTrack).setSequence(midiSequence);    // DIRTY !
        }
    }

    /**
     * Start or pause a track
     */
    public boolean togglePause() {
        return PlayerTrack.getFacadePlayerTrackInstance().togglePause();
    }

    /**
     * Stop a track being played
     */
    public void stop() {
        PlayerTrack.getFacadePlayerTrackInstance().stop();
    }

    /**
     * Register an observer
     */
    public void registerObserver(Observer obs) {
	PlayerTrack.getFacadePlayerTrackInstance().addObserver(obs);
    }

    public final static FacadePlayerMidiTrack getFacadePlayerTrackInstance(Sequence seq) {
	if (PlayerTrack.facadePlayerTrack == null) {
	    return new FacadePlayerMidiTrack(seq);
	} else {
	    return (FacadePlayerMidiTrack) PlayerTrack.facadePlayerTrack;
	}
    }

    public final static FacadePlayerMidiTrack getFacadePlayerTrackInstance() {
	return (FacadePlayerMidiTrack) PlayerTrack.facadePlayerTrack;
    }
}