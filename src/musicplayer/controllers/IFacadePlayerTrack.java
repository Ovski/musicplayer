package musicplayer.controllers;

import java.util.Observer;

/**
 * IFacadePlayerTrack
 * 
 * Interface to use for facades for playing a music in a certain format
 * 
 * @author baptiste
 */
public interface IFacadePlayerTrack {

    /**
     * Stop a track being played
     */
    public void stop();

    /**
     * Start or pause a track
     */
    public boolean togglePause();

    /**
     * Get the position of the track
     */
    public long getPosition();

    /**
     * Add an observer
     * @param obs
     */
	public void addObserver(Observer obs);
}