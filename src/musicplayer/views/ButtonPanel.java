package musicplayer.views;

import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import musicplayer.controllers.PlayerTrack;
import musicplayer.views.listeners.GenerateListener;
import musicplayer.views.listeners.LoadFromServerListener;
import musicplayer.views.listeners.LoadListener;
import musicplayer.views.listeners.PlayListener;
import musicplayer.views.listeners.StopListener;

public class ButtonPanel extends JPanel implements Observer {

    private static final long serialVersionUID = 1L;
    private ActionListener playListener;
    private ActionListener stopListener;
    private ActionListener loadListener;
    private ActionListener generateListener;
    private ActionListener loadFromServerListener;
    private JButton playButton;
    private JButton stopButton;
    private JButton loadButton;
    private JButton loadFromServerButton;
    private JButton generateButton;
    private JComboBox<String> behList;
    private PlayerTrack playerTrack;

    /**
     * Constructor
     */
    public ButtonPanel(PlayerTrack playerTrack) {
	this.playerTrack = playerTrack;
	this.initPanel();
	this.addListeners();
	
    }

    private void initPanel() {
	this.playButton = new JButton("Play");
    	this.stopButton = new JButton("Stop");
    	this.loadButton = new JButton("Load");
    	this.generateButton = new JButton("Generate");
    	this.loadFromServerButton = new JButton("Load from server");
        String[] beh = { "Rectangle", "Circle", "Polygon" };
	this.behList = new JComboBox<String>(beh);
	this.behList.setSelectedIndex(0);
	this.add(playButton);
    	this.add(stopButton);
    	this.add(generateButton);
    	this.add(loadButton);
    	this.add(loadFromServerButton);
        this.add(behList);
    }

    private void addListeners() {
	this.playListener = new PlayListener(this.playerTrack);
	this.stopListener = new StopListener(this.playerTrack, this.playButton);
	this.loadListener = new LoadListener(this.playerTrack);
	this.loadFromServerListener = new LoadFromServerListener(this.playerTrack);
	this.generateListener = new GenerateListener(this.playerTrack);
	this.playButton.addActionListener(this.playListener);
	this.stopButton.addActionListener(this.stopListener);
    	this.loadButton.addActionListener(this.loadListener);
    	this.loadFromServerButton.addActionListener(this.loadFromServerListener);
    	this.generateButton.addActionListener(this.generateListener);
    }

    @Override
    public void update(Observable o, Object arg) {
	if (arg.toString() == "END") {
	    this.playerTrack.stop();
	    this.playButton.setText("Play");
	    repaint();
	}
    }
}
