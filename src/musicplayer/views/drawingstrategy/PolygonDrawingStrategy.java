package musicplayer.views.drawingstrategy;

import java.awt.Color;
import java.awt.Graphics;

public class PolygonDrawingStrategy implements IdrawingStrategy {

    private int panelDimension;

    /**
     * Constructor
     * 
     * @param int panelDimension
     */
    public PolygonDrawingStrategy(int panelDimension) throws Exception {
	if (panelDimension < 200) {
	    throw new Exception("The panel dimension must be at least 200 or higher");
	}
	this.panelDimension = panelDimension;
    }
    
    @Override
    public void draw(Graphics g) {
	int r = (int) (Math.random() * 250);
	int gr = (int) (Math.random() * 250);
	int b = (int) (Math.random() * 250);

	g.setColor(new Color(r, gr, b));

	int pointNumber = 5;
	int[] x = new int[pointNumber];
	int[] y = new int[pointNumber];
	for (int i = 0; i<pointNumber; i++) {
        	x[i] = (int) ((Math.random() * this.panelDimension) + 100);
        	y[i] = (int) ((Math.random() * this.panelDimension) + 100);
	}

	g.fillPolygon(x, y, pointNumber);
    }
}
