package musicplayer.views.drawingstrategy;

import java.awt.Graphics;

public interface IdrawingStrategy {
    public void draw(Graphics g);
}