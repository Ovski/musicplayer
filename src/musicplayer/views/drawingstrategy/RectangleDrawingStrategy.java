package musicplayer.views.drawingstrategy;

import java.awt.Color;
import java.awt.Graphics;

public class RectangleDrawingStrategy implements IdrawingStrategy {

    private int panelDimension;

    /**
     * Constructor
     * 
     * @param int panelDimension
     */
    public RectangleDrawingStrategy(int panelDimension) throws Exception {
	if (panelDimension < 200) {
	    throw new Exception("The panel dimension must be at least 200 or higher");
	}
	this.panelDimension = panelDimension;
    }
    
    @Override
    public void draw(Graphics g) {
	int r = (int) (Math.random() * 250);
	int gr = (int) (Math.random() * 250);
	int b = (int) (Math.random() * 250);

	g.setColor(new Color(r, gr, b));

	int ht = (int) ((Math.random() * this.panelDimension/2) + 10);
	int width = (int) ((Math.random() * this.panelDimension/2) + 10);

	int x = (int) ((Math.random() * 50) + 100);
	int y = (int) ((Math.random() * 50) + 100);

	g.fillRect(x, y, ht, width);
    }
}
