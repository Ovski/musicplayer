package musicplayer.views.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import musicplayer.controllers.PlayerTrack;

/**
 * LoadListener
 * 
 * Listener on the load button
 * 
 * @author pierre-jean leger
 */
public class GenerateListener implements ActionListener {

	private PlayerTrack playerTrack;
	
	public GenerateListener(PlayerTrack playerTrack) {
		super();
		this.playerTrack = playerTrack;
	}
	
    @Override
    public void actionPerformed(ActionEvent e) {
    	this.playerTrack.load(1);
    }
}
