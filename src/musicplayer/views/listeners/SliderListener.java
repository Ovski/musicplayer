package musicplayer.views.listeners;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import musicplayer.controllers.PlayerTrack;

/**
 * SliderListener
 * 
 * Listener on the slider
 * 
 * @author pierre-jean leger
 */
public class SliderListener implements ChangeListener {

	//private PlayerTrack playerTrack;
	
	public SliderListener(PlayerTrack playerTrack) {
		super();
		//this.playerTrack = playerTrack;
	}
	
    @Override
    public void stateChanged(ChangeEvent e) {
    	
    }

}
