package musicplayer.views.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import musicplayer.views.PlayerTrackPanel;
import musicplayer.views.drawingstrategy.CircleDrawingStrategy;
import musicplayer.views.drawingstrategy.PolygonDrawingStrategy;
import musicplayer.views.drawingstrategy.RectangleDrawingStrategy;

public class DrawingStrategyListener implements ActionListener {

    private PlayerTrackPanel playerTrackPanel;

    public DrawingStrategyListener(PlayerTrackPanel playerTrackPanel) {
	this.playerTrackPanel = playerTrackPanel;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void actionPerformed(ActionEvent e) {
	try {
        	switch ((String) ((JComboBox<String>) e.getSource()).getSelectedItem()) {
        	case "Rectangle":
        	    this.playerTrackPanel
        		    .setDrawingStrategy(new RectangleDrawingStrategy(this.playerTrackPanel.getPanelDimension()));
        	    break;
        	case "Circle":
        	    this.playerTrackPanel
        		    .setDrawingStrategy(new CircleDrawingStrategy(this.playerTrackPanel.getPanelDimension()));
        	    break;
        	case "Polygon":
        	    this.playerTrackPanel
        		    .setDrawingStrategy(new PolygonDrawingStrategy(this.playerTrackPanel.getPanelDimension()));
        	    break;
        	}
	} catch (Exception exc) {
	    exc.printStackTrace();
	}
    }
}
