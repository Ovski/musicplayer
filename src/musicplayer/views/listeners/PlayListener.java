package musicplayer.views.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import musicplayer.controllers.PlayerTrack;

/**
 * PlayListener
 * 
 * Listener on the play button
 * 
 * @author pierre-jean leger
 */
public class PlayListener implements ActionListener {

	private PlayerTrack playerTrack;
	
	public PlayListener(PlayerTrack playerTrack) {
		super();
		this.playerTrack = playerTrack;
	}
	
    @Override
    public void actionPerformed(ActionEvent e) {
    	boolean status = this.playerTrack.togglePause();
    	// Toggle the jbutton text
    	if(status) {
    		((JButton)e.getSource()).setText("Pause");
    	}
    	else {
    		((JButton)e.getSource()).setText("Play");
    	}
    }
}