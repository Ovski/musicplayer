package musicplayer.views.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import musicplayer.controllers.PlayerTrack;

/**
 * StopListener
 * 
 * Listener on the stop button
 * 
 * @author pierre-jean leger
 */
public class StopListener implements ActionListener {

    private PlayerTrack playerTrack;
    private JButton playButton;

    public StopListener(PlayerTrack playerTrack, JButton playButton) {
	super();
	this.playerTrack = playerTrack;
	this.playButton = playButton;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
	this.playerTrack.stop();
	this.playButton.setText("Play");
    }
}
