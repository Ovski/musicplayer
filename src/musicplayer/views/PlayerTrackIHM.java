package musicplayer.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.*;
import musicplayer.controllers.PlayerTrack;
import musicplayer.views.listeners.DrawingStrategyListener;

/**
 * PlayerTrackIHM
 * 
 * The player IHM
 * 
 * @author pierre_jean leger
 */
public class PlayerTrackIHM extends JFrame {

    private static final long serialVersionUID = 6814519493845440198L;
    private PlayerTrack playerTrack;
    private PlayerTrackPanel playerTrackPanel;
    private ButtonPanel buttonPanel;
    private DrawingStrategyListener drawingStrategyListener;

    public PlayerTrackIHM() {
	super();
	this.playerTrack = new PlayerTrack();
	this.initPanels();
	this.addListeners();
	this.initFrameComponents();
	this.playerTrack.registerObserver(buttonPanel);
	this.playerTrack.registerObserver(playerTrackPanel);
    }

    private void initFrameComponents() {
	this.setTitle("PlayerTrack");
	this.setSize(600, 400);
	this.setResizable(false);
	this.setLocationRelativeTo(null);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setVisible(true);
    }

    private void initPanels() {
	this.playerTrackPanel = new PlayerTrackPanel();
	this.buttonPanel = new ButtonPanel(this.playerTrack);
	Container pane = this.getContentPane();
	pane.setLayout(new BorderLayout());
	this.buttonPanel.setBackground(Color.blue);
	pane.add(this.buttonPanel, BorderLayout.NORTH);
	pane.add(this.playerTrackPanel, BorderLayout.CENTER);
    }

    private void addListeners() {
	this.drawingStrategyListener = new DrawingStrategyListener(this.playerTrackPanel);
    }

    /**
     * @return the playerTrack
     */
    /*public PlayerTrack getPlayerTrack() {
        return playerTrack;
    }*/

    /**
     * @param playerTrack the playerTrack to set
     */
    /*public void setPlayerTrack(PlayerTrack playerTrack) {
        this.playerTrack = playerTrack;
    }*/
}
