package musicplayer.views;

import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import musicplayer.views.drawingstrategy.IdrawingStrategy;
import musicplayer.views.drawingstrategy.RectangleDrawingStrategy;

public class PlayerTrackPanel extends JPanel implements Observer {

    private int panelDimension;
    private IdrawingStrategy drawingStrategy;
    private static final long serialVersionUID = 7051968232221029693L;
    private boolean dirty;

    /**
     * Constructor
     */
    public PlayerTrackPanel() {
	this.panelDimension = 400;
	try {
	    this.drawingStrategy = new RectangleDrawingStrategy(this.panelDimension);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
    /**
     * Constructor
     */
    public PlayerTrackPanel(int panelDimension) {
	this();
	this.panelDimension = panelDimension;
    }
    
    /**
     * @return the panelDimension
     */
    public int getPanelDimension() {
        return panelDimension;
    }

    /**
     * @param panelDimension the panelDimension to set
     */
    public void setPanelDimension(int panelDimension) {
        this.panelDimension = panelDimension;
    }

   // public void paintComponent(Graphics g) {
   public void paintComponent(Graphics g) {
	if (dirty) {
	    drawingStrategy.draw(g);
	    dirty = false;
	}
    }

    @Override
    public void update(Observable o, Object arg) {
	dirty = true;
	repaint();
    }

    /**
     * @return the drawingStrategy
     */
    public IdrawingStrategy getDrawingStrategy() {
        return drawingStrategy;
    }

    /**
     * @param drawingStrategy the drawingStrategy to set
     */
    public void setDrawingStrategy(IdrawingStrategy drawingStrategy) {
        this.drawingStrategy = drawingStrategy;
    }
}
