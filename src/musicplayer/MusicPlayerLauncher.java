package musicplayer;

import musicplayer.views.PlayerTrackIHM;

/**
 * MusicPlayerLauncher
 * 
 * Application Launcher
 * 
 * @author baptiste
 */
public class MusicPlayerLauncher {

	@SuppressWarnings("unused")
	private static PlayerTrackIHM player;

	// private static PlayerTrackIHM playerIHM;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launchPlayer();
	}

	private static void launchPlayer() {
		// playerIHM = new PlayerTrackIHM();
		player = new PlayerTrackIHM();
	}
}