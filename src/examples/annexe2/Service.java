package examples.annexe2;

/**
 * @author Kathy Sierra, Bert Bates : "Java T�te la Premi�re" 
 */
import javax.swing.*;
import java.io.*;

public interface Service extends Serializable {
	public JPanel getIHM();
}
