package musicserver.service;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by caligone on 10/10/14.
 */
public interface IMusicService extends Remote {

    public byte[] getMidiOfTheDay() throws RemoteException;
}
